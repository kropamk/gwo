(function () {
  $(document).ready(function () {
    $('.c-slider').slick({
      autoplay: true,
      arrows: true
    });

    $('.navbar-toggle').on('click', function () {
      var $this = $(this);
      var $userPanel = $('.user-panel');

      if ($this.hasClass('active')) {
        $this.find('.cross').fadeOut(200, function(){
          $this.find('.default').fadeIn(200);
        });
        $userPanel.stop().slideUp(300, function () {
          $this.removeClass('active');
        });
      } else {
        $this.find('.default').fadeOut(200, function(){
          $this.find('.cross').fadeIn(200);
        });
        $userPanel.stop().slideDown(300, function () {
          $this.addClass('active');
        });
      }
    });
  });
})();
