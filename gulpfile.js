var gulp = require('gulp');
var sourcemaps = require('gulp-sourcemaps');
var less = require('gulp-less');
var LessAutoprefix = require('less-plugin-autoprefix');
var autoprefix = new LessAutoprefix({
  browsers: ['last 2 versions']
});
var browserSync = require('browser-sync').create();

var path = {
  files: ['*.php', 'templates/**/*.twig'],
  less: {
    src: 'assets/less/',
    dist: 'assets/css/'
  }
}

gulp.task('serve', ['less'], function () {
  browserSync.init({
    proxy: "localhost/gwo/zadanie"
  });

  gulp.watch(path.less.src + '**/*.less', ['less']);
  gulp.watch(path.files).on('change', browserSync.reload);
});

gulp.task('less', function () {
  return gulp.src(path.less.src + 'main.less')
    .pipe(sourcemaps.init())
    .pipe(less({
      plugins: [autoprefix]
    }))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(path.less.dist))
    .pipe(browserSync.stream())
});
